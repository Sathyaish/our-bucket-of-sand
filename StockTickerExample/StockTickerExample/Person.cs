﻿using System;

namespace StockTickerExample
{
    public class Person
    {
        public void PriceChangedHandler(string name, decimal oldPrice, decimal newPrice)
        {
            Console.WriteLine("The price of {0} stock changed from {1} to {2}.",
                name, oldPrice, newPrice);
        }
    }
}

﻿
namespace StockTickerExample
{
    public class StockTicker
    {
        public PriceChanged PriceChanged;
        private decimal _price;

        public string Name { get; set; }
        
        public decimal Price 
        { 
            get
            {
                return _price;
            }
            set
            {
                var oldPrice = _price;
                _price = value;

                if (PriceChanged != null)
                {
                    PriceChanged(this.Name, oldPrice, _price);
                }
            }
        }


    }
}
﻿
namespace StockTickerExample
{
    public delegate void PriceChanged(string name, decimal oldPrice, decimal newPrice);
}

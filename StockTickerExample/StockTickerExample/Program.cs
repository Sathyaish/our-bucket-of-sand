﻿using System;

namespace StockTickerExample
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person();

            var microsoft = new StockTicker { Name = "Microsoft Corporation", Price = 22m };
            microsoft.PriceChanged = microsoft.PriceChanged + new PriceChanged(person.PriceChangedHandler);

            microsoft.Price--;

            Console.ReadKey();
        }
    }
}
